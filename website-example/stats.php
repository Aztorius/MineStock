<?php
$name_of_item = $_GET["name"];

$bdd = new PDO('mysql:host=localhost;dbname=minestock;charset=utf8', 'root', '');

$query_name = $bdd->query("SELECT DISTINCT name FROM ms_global");

?>

<form method="statistiques.php" method="get">
    <p>
        <label for="jour">Name of the item : </label>
        <select name="name" id="name" onchange="document.location.href = this.value;">
            <option>Name</option>
        <?php
            while($result = $query_name->fetch(PDO::FETCH_ASSOC)){
            echo "<option value=stats.php?name=".str_replace(" " , "%20",$result['name']).">".$result['name']."</option>";
            }
        ?>
        </select>
    </p>
</form>

<?php
$sth = $bdd->query("SELECT * FROM ms_global WHERE name='".$name_of_item."'");

$rows = array();
//flag is not needed
$flag = true;
$table = array();
$table['cols'] = array(
 
   // Labels for your chart, these represent the column titles
   array('label' => 'date', 'type' => 'datetime'),
   array('label' => 'stock '.$name_of_item, 'type' => 'number')

);

$table2['cols'] = array(
    array('label' => 'date', 'type' => 'datetime'),
    array('label' => 'price '.$name_of_item, 'type' => 'number'),
    array('label' => 'sell price '.$name_of_item, 'type' => 'number')
);
 
$rows = array();
$rows2 = array();

while($r = $sth->fetch(PDO::FETCH_ASSOC)) {
    $temp = array();
    $temp2 = array();
    
    // the following line will be used in the linechart
    $time = strtotime($r['time_date']);
    $newformattime = date('Y,m,d,H,i,s',$time);
    $finaltime = mktime(date('H',$time), date('i',$time), date('s',$time), date('n',$time)-1, date('d',$time), date('Y',$time));
    $newformattime2 = date('Y,m,d,H,i,s',$finaltime);
    $temp[] = array('v' => (string) "Date(".$newformattime2.")");
    $temp2[] = array('v' => (string) "Date(".$newformattime2.")");
    
    // Values of each column
    $temp[] = array('v' => (int) $r['global_stock']);
    $temp2[] = array('v' => (double) $r['global_price_buy']);
    $temp2[] = array('v' => (double) $r['global_price_sell']);
    
    $rows[] = array('c' => $temp);
    $rows2[] = array('c' => $temp2);
}
 
$table['rows'] = $rows;
$table2['rows'] = $rows2;

$jsonTable = json_encode($table);
$jsonTable2 = json_encode($table2);
?>
 
<html>
  <head>
    <!--Load the Ajax API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script type="text/javascript">
 
    // Load the Visualization API and the linechart package.
    google.load('visualization', '1', {'packages':['corechart']});
 
    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);
 
    function drawChart() {
 
      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(<?=$jsonTable?>);
      var data2 = new google.visualization.DataTable(<?=$jsonTable2?>);
      
      var options = {
          title: ' <?=$name_of_item?>',
          //curveType: 'function',
          is3D: 'true',
          vAxes: {0: {logScale: false},
            1: {logScale: false, maxValue: 2}},
          series: {0: {targetAxisIndex:0},
                   1:{targetAxisIndex:1},
                   2:{targetAxisIndex:1},
                  },
          width: 800,
          height: 600
        };
      // Instantiate and draw our chart, passing in some options.
      // Do not forget to check your div ID
      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
      chart.draw(data, options);
      var chart2 = new google.visualization.LineChart(document.getElementById('chart_div2'));
      chart2.draw(data2, options);
    }
    </script>
  </head>
 
  <body>
    <!--this is the div that will hold the line chart-->
    <div style="display: inline-block" id="chart_div"></div>
    <div style="display: inline-block" id="chart_div2"></div>
  </body>
</html>