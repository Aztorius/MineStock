# MineStock

MineStock is a bukkit/spigot plugin wich can change the price of objects in BetterShops' shops.

[Bukkit Dev page](https://dev.bukkit.org/projects/minestock)

Important : since BetterShops is no longer maintained (since 2015), the plugin MineStock is in idle mode.
If you have an issue or want to contribute, no problem, you can add an issue or a pull request.
If I have a great idea for the plugin and the time to make it real, I will do it.
At the moment MineStock works and without issue.

Dependencies
------------
BetterShops 2.0+ (https://www.spigotmc.org/resources/better-shops.1758/)
