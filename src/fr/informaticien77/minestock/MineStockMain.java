package fr.informaticien77.minestock;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import max.hubbard.bettershops.ShopManager;
import max.hubbard.bettershops.Events.ShopBuyItemEvent;
import max.hubbard.bettershops.Events.ShopSellItemEvent;
import max.hubbard.bettershops.Shops.History;
import max.hubbard.bettershops.Shops.Shop;
import max.hubbard.bettershops.Shops.Items.ShopItem;
import max.hubbard.bettershops.Utils.Transaction;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import fr.informaticien77.minestock.jfep.Parser;
import fr.informaticien77.minestock.mysql.mysql.MySQL;

public class MineStockMain extends JavaPlugin implements Listener{

	MySQL mysql = new MySQL(this, getConfig().getString("sql-host"), getConfig().getString("sql-port"), getConfig().getString("sql-database"), getConfig().getString("sql-username"), getConfig().getString("sql-password"));
	Connection c = null;

	private FileConfiguration configitemsname = null;
	private File fileitemsname = null;

	public Logger log = Logger.getLogger("Minecraft");

	@Override
	public void onEnable(){
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(this,this);

		getConfig().options().copyDefaults(true);
		saveDefaultConfig();

		Parser equation_selling, equation_buying;
        String equationInput_selling = getConfig().getString("selling-price-equation");
        String equationInput_buying = getConfig().getString("buying-price-equation");

		try {
            equation_selling = new Parser(equationInput_selling);

            // test equation
            equation_selling.setVariable("stock", 1);
            equation_selling.setVariable("price", 1);
            equation_selling.setVariable("amount", 1);
            equation_selling.getValue();
        } catch(Exception e) {
            getLogger().warning("MineStock has an invalid selling equation property.");
        }

		try {
            equation_buying = new Parser(equationInput_buying);

            // test equation
            equation_buying.setVariable("stock", 1);
            equation_buying.setVariable("price", 1);
            equation_buying.setVariable("amount", 1);
            equation_buying.getValue();
        } catch(Exception e) {
            getLogger().warning("MineStock has an invalid buying equation property.");
        }

		fileitemsname = new File(getDataFolder() + "/itemsname.yml");
		if(!fileitemsname.exists()) {
	        try {
				fileitemsname.createNewFile();
			} catch (IOException e) {
				getLogger().warning("Unable to write the itemsname.yml file.");
			}
	    }
		configitemsname = YamlConfiguration.loadConfiguration(fileitemsname);
		Reader configStreamitemsname = null;
		try {
			configStreamitemsname = new InputStreamReader(this.getResource("itemsname.yml"), "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			getLogger().severe("Error when loading the default itemsname.yml file.");
		}

		YamlConfiguration defaultConfigitemsname = YamlConfiguration.loadConfiguration(configStreamitemsname);
        configitemsname.setDefaults(defaultConfigitemsname);
        configitemsname.options().copyDefaults(true);
        try {
			configitemsname.save(fileitemsname);
		} catch (IOException e1) {
			getLogger().warning("Unable to save the itemsname.yml config file.");
		}

		if(getConfig().getBoolean("sql")){
			boolean ok = false;
			try {
				c = mysql.openConnection();
				getLogger().info("SQL enable");
				ok = true;
			} catch (ClassNotFoundException e) {
				getLogger().warning("Unable to connect to the SQL database. SQL disabled.");
				getConfig().set("sql", false);
				saveConfig();
			} catch (SQLException e) {
				getLogger().warning("Unable to connect to the SQL database. SQL disabled.");
				getConfig().set("sql", false);
				saveConfig();
			}

			if(ok){
				Statement statement;
				try {
					statement = c.createStatement();
					statement.executeUpdate("CREATE TABLE IF NOT EXISTS `ms_global` (id int PRIMARY KEY AUTO_INCREMENT, name varchar(64), bukkit_name varchar(64), mc_sub_id int, global_price_buy double, global_price_sell double, global_stock int, time_date datetime);");
				} catch (SQLException e) {
					getLogger().warning("Unable to create the SQL table 'ms_global'.");
				}
			}
		}

		if(getConfig().getBoolean("sendInformationsWithMetrics")){
			try {
				Metrics metrics = new Metrics(this);
				metrics.start();
				getLogger().info("Metrics enabled.");
			} catch (IOException e) {
				// Failed to submit the stats :-(
				getLogger().warning("MineStock failed to send statistics.");
			}
		}
		
		if(getConfig().getBoolean("prices-decrease-with-time")){
			long refresh_time = getConfig().getLong("refresh-time") * 20;
			BukkitRunnable task = new UpdatePricesWithTime();
			task.runTaskTimer(this, 600, refresh_time);
			getLogger().info("Prices decrease with time.");
		}
	}

	public void onDisable(){

	}
	
	public class UpdatePricesWithTime extends BukkitRunnable {
		   @Override
		   public void run() {
			   Date current_date = new Date();
			   long time_limit = getConfig().getLong("time-limit") * 1000;
			   for (Shop shop : ShopManager.getShops()){
				   History history = shop.getHistory();
				   LinkedList<String> already_checked_items = new LinkedList<String>();
				   for (Transaction transaction : history.getBuyingTransactions()){
					   String current_item = transaction.getItem();
					   Date date_last_transaction = transaction.getDate();
					   if ((!already_checked_items.contains(current_item)) && (current_date.getTime() - date_last_transaction.getTime() > time_limit)){
						   //We need to decrease the current price
						   for (ShopItem item : shop.getShopItems()){
							   if (current_item.equals(item.getItem().getType().name())){
								   double price_before = item.getPrice();
								   double price_after = Math.floor((price_before - Math.log(Math.log(1 + price_before))) * 100) / 100;
								   
								   if (price_after < item.getOrigPrice()){
									   price_after = item.getOrigPrice();
								   }
								   item.setPrice(price_after);
								   
								   String realname;
								   if(configitemsname.getString(current_item + "-" + item.getDurability()) != null){
									   realname = configitemsname.getString(current_item + "-" + item.getDurability());
								   }
								   else if(configitemsname.getString(current_item + "-0") != null){
									   realname = configitemsname.getString(current_item + "-0");
								   }
								   else{
									   configitemsname.set(current_item + "-" + item.getItem().getDurability(), current_item);
									   realname = current_item;
								   }
								   
								   String message = getConfig().getString("messageChangedPrices");
								   message = message.replaceAll("%shopname%", shop.getName());
								   message = message.replaceAll("%item%", realname);
								   message = message.replaceAll("%prices%", Double.toString(price_after));
								   message = message.replace('&', '§');
								   Bukkit.broadcastMessage(message);
							   }
						   }
						   already_checked_items.add(current_item);
					   }
				   }
			   }
			   Bukkit.broadcastMessage("[MineStock] Prices updated");
		   	}
	}

	@EventHandler
	public void onBuyItem(ShopBuyItemEvent event){
	    Shop shop = event.getShop();
	    ShopItem item = event.getItem();
	    ShopItem itemsell = item.getSister();

	    History historique = shop.getHistory();
	    LinkedList<Transaction> listetrans = historique.getBuyingTransactions();
	    Transaction trans = (Transaction) listetrans.get(listetrans.size()-1);

	    if(item.getLiveEco()){
	    	getLogger().warning("LiveEconomy should be disabled");
	    }

	    int stock = 0, globalstock = 0;
	    int countbuy = 0, countsell = 0;
	    double globalbuyprice = 0, globalsellprice = 0;

	    for (Shop shops : ShopManager.getShops()){
	    	List<ShopItem> items = shops.getShopItems(true);
	    	List<ShopItem> itemb = shops.getShopItems(false);

	    	for(int i = 0; i < itemb.size(); i++){
	    		if(itemb.get(i).getItem().isSimilar(item.getItem())){
		   			globalstock += itemb.get(i).getStock();
		   			globalbuyprice += itemb.get(i).getPrice();
		   			countbuy++;
		   		}
	    	}

	    	for(int i = 0; i < items.size(); i++){
	    		if(items.get(i).getItem().isSimilar(item.getItem())){
		   			globalstock += items.get(i).getStock();
		   			globalsellprice += items.get(i).getPrice();
		   			countsell++;
		   		}
	    	}

	    }

	    if(countbuy > 0){
	    	globalbuyprice /= countbuy;
	    	globalbuyprice = ((double)Math.floor(globalbuyprice*100.0))/100;
	    }

	    if(countsell > 0){
	    	globalsellprice /= countsell;
	    	globalsellprice = ((double)Math.floor(globalsellprice*100.0))/100;
	    }

	    if(getConfig().getBoolean("global-stock")){
	    	stock = globalstock;
	    }
	    else{
	    	stock = item.getStock();

	    	if(itemsell != null){
		    	stock += itemsell.getStock();
		    }
	    }

	    int amountbuy = trans.getAmount();
	    double percentprice = getConfig().getDouble("percent-sell-price");
	    double price = 0;

	    ItemStack itembuy = item.getItem();
	    String realname;
	    if(configitemsname.getString(itembuy.getType().name() + "-" + itembuy.getDurability()) != null){
	    	realname = configitemsname.getString(itembuy.getType().name() + "-" + itembuy.getDurability());
	    }
	    else if(configitemsname.getString(itembuy.getType().name() + "-0") != null){
	    	realname = configitemsname.getString(itembuy.getType().name() + "-0");
	    }
	    else{
	    	configitemsname.set(itembuy.getType().name() + "-" + itembuy.getDurability(), itembuy.getType().name());
	    	realname = itembuy.getType().name();
	    }

	    String buystockmessage = getConfig().getString("messageChangedStock");
	    buystockmessage = buystockmessage.replaceAll("%stock%", Integer.toString(stock));
	    buystockmessage = buystockmessage.replaceAll("%item%", realname);
	    buystockmessage = buystockmessage.replace('&', '§');
	    Bukkit.broadcastMessage(buystockmessage);

	    if(item.getStock() == 0){
	    	String buyoutmessage = getConfig().getString("messageOutOfStock");
		    buyoutmessage = buyoutmessage.replaceAll("%shopname%", shop.getName());
		    buyoutmessage = buyoutmessage.replaceAll("%item%", realname);
		    buyoutmessage = buyoutmessage.replace('&', '§');
		    Bukkit.broadcastMessage(buyoutmessage);
	    }
	    else{
	    	price = item.getPrice();

	    	Parser equation;
            String equationInput = getConfig().getString("buying-price-equation");

            equation = new Parser(equationInput);

            equation.setVariable("stock", stock);
            equation.setVariable("price", price);
            equation.setVariable("amount", amountbuy);
            double newprice = equation.getValue();

		    if(newprice < item.getMinPrice()){
		    	newprice = item.getMinPrice();
		    }
		    double finalprice = ((double)Math.floor(100.0*newprice))/100;
		    item.setPrice(finalprice);
		    String prices = Double.toString(finalprice);
		    prices += "/";
		    if(itemsell != null){
		    	double finalsellprice = ((double)Math.floor(percentprice*finalprice))/100;
		    	itemsell.setPrice(finalsellprice);
		    	prices += Double.toString(finalsellprice);
		    }
		    else{
		    	prices += "-";
		    }
		    String buymessage = getConfig().getString("messageChangedPrices");
		    buymessage = buymessage.replaceAll("%shopname%", shop.getName());
		    buymessage = buymessage.replaceAll("%item%", realname);
		    buymessage = buymessage.replaceAll("%prices%", prices);
		    buymessage = buymessage.replace('&', '§');
		    Bukkit.broadcastMessage(buymessage);
	    }

	    if(getConfig().getBoolean("sql")){
	    	Statement statement;
			try {
				statement = c.createStatement();
				statement.executeUpdate("INSERT INTO ms_global (`bukkit_name`, `mc_sub_id`, `name`, `global_price_buy`, `global_price_sell`, `global_stock`, `time_date`) VALUES ('" + itembuy.getType().name() + "', '" + itembuy.getDurability() + "', '" + realname + "', '" + globalbuyprice + "', '" + globalsellprice + "', '" + stock + "', NOW());");
			} catch (SQLException e1) {
				getLogger().warning("Unable to send datas to the SQL database.");
			}
	    }

	}

	@EventHandler
	public void onSellItem(ShopSellItemEvent event){
		Shop shop = event.getShop();
	    ShopItem item = event.getItem();
	    ShopItem itembuy = item.getSister();

	    History historique = shop.getHistory();
	    LinkedList<Transaction> listetrans = historique.getSellingTransactions();
	    Transaction trans = (Transaction) listetrans.get(listetrans.size()-1);

	    if(item.getLiveEco()){
	    	getLogger().warning("LiveEconomy should be disabled");
	    }

	    int stock = 0, globalstock = 0;
	    int countbuy = 0, countsell = 0;
	    double globalbuyprice = 0, globalsellprice = 0;

	    for (Shop shops : ShopManager.getShops()){
	    	List<ShopItem> items = shops.getShopItems(true);
	    	List<ShopItem> itemb = shops.getShopItems(false);

	    	for(int i = 0; i < itemb.size(); i++){
	    		if(itemb.get(i).getItem().isSimilar(item.getItem())){
		   			globalstock += itemb.get(i).getStock();
		   			globalbuyprice += itemb.get(i).getPrice();
		   			countbuy++;
		   		}
	    	}

	    	for(int i = 0; i < items.size(); i++){
	    		if(items.get(i).getItem().isSimilar(item.getItem())){
		   			globalstock += items.get(i).getStock();
		   			globalsellprice += items.get(i).getPrice();
		   			countsell++;
		   		}
	    	}
	    }

	    if(countbuy > 0){
	    	globalbuyprice /= countbuy;
	    }

	    if(countsell > 0){
	    	globalsellprice /= countsell;
	    }

	    if(getConfig().getBoolean("global-stock")){
	    	stock = globalstock;
	    }
	    else{
	    	stock = item.getStock();

	    	if(itembuy != null){
		    	stock += itembuy.getStock();
		    }
	    }

	    int amountsell = trans.getAmount();
	    double percentprice = getConfig().getDouble("percent-sell-price");

	    ItemStack itemsell = item.getItem();
	    String realname;
	    if(configitemsname.getString(itemsell.getType().name() + "-" + itemsell.getDurability()) != null){
	    	realname = configitemsname.getString(itemsell.getType().name() + "-" + itemsell.getDurability());
	    }
	    else if(configitemsname.getString(itemsell.getType().name() + "-0") != null){
	    	realname = configitemsname.getString(itemsell.getType().name() + "-0");
	    }
	    else{
	    	configitemsname.set(itemsell.getType().name() + "-" + itemsell.getDurability(), itemsell.getType().name());
	    	realname = itemsell.getType().name();
	    }

	    String sellstockmessage = getConfig().getString("messageChangedStock");
	    sellstockmessage = sellstockmessage.replaceAll("%stock%", Integer.toString(stock));
	    sellstockmessage = sellstockmessage.replaceAll("%item%", realname);
	    sellstockmessage = sellstockmessage.replace('&', '§');
	    Bukkit.broadcastMessage(sellstockmessage);

	    if(stock <= 1){
	    	double price = item.getPrice();
		    double newprice = price;
		    if(newprice < item.getMinPrice()){
		    	newprice = item.getMinPrice();
		    }
		    double finalprice = ((double)Math.floor(100.0*newprice))/100;
		    item.setPrice(finalprice);

		    String prices = "";
		    if(itembuy != null){
		    	prices += Double.toString(itembuy.getPrice());
		    }
		    else{
		    	prices += "-";
		    }
		    prices += "/";
		    prices += Double.toString(finalprice);

		    String sellmessage = getConfig().getString("messageChangedPrices");
		    sellmessage = sellmessage.replaceAll("%shopname%", shop.getName());
		    sellmessage = sellmessage.replaceAll("%item%", realname);
		    sellmessage = sellmessage.replaceAll("%prices%", prices);
		    sellmessage = sellmessage.replace('&', '§');
		    Bukkit.broadcastMessage(sellmessage);
	    }
	    else{
	    	String prices = "";

	    	Parser equation;
            String equationInput = getConfig().getString("selling-price-equation");

            equation = new Parser(equationInput);
            
            equation.setVariable("stock", stock);
            equation.setVariable("amount", amountsell);

	    	if(itembuy != null){
	    		double price = itembuy.getPrice();

                equation.setVariable("price", price);

	            double newprice = equation.getValue();

			    if(newprice < itembuy.getMinPrice()){
			    	newprice = itembuy.getMinPrice();
			    }
			    double finalprice = ((double)Math.floor(100.0*newprice))/100;
			    itembuy.setPrice(finalprice);

			    double finalpricesell = ((double)Math.floor(percentprice*finalprice))/100;
			    item.setPrice(finalpricesell);

			    prices += Double.toString(finalprice);
			    prices += "/";
			    prices += Double.toString(finalpricesell);
	    	}
	    	else{
	    		double price = item.getPrice();

                equation.setVariable("price", price);

	            double newprice = equation.getValue();

			    if(newprice < item.getMinPrice()){
			    	newprice = item.getMinPrice();
			    }
			    double finalprice = ((double)Math.floor(100.0*newprice))/100;
			    item.setPrice(finalprice);

			    prices += "-";
			    prices += "/";
			    prices += Double.toString(finalprice);
	    	}

		    String sellmessage = getConfig().getString("messageChangedPrices");
		    sellmessage = sellmessage.replaceAll("%shopname%", shop.getName());
		    sellmessage = sellmessage.replaceAll("%item%", realname);
		    sellmessage = sellmessage.replaceAll("%prices%", prices);
		    sellmessage = sellmessage.replace('&', '§');
		    Bukkit.broadcastMessage(sellmessage);

		    if(getConfig().getBoolean("sql")){
		    	Statement statement;
				try {
					statement = c.createStatement();
					statement.executeUpdate("INSERT INTO ms_global (`bukkit_name`, `mc_sub_id`, `name`, `global_price_buy`, `global_price_sell`, `global_stock`, `time_date`) VALUES ('" + itemsell.getType().name() + "', '" + itemsell.getDurability() + "', '" + realname + "', '" + globalbuyprice + "', '" + globalsellprice + "', '" + stock + "', NOW());");
				} catch (SQLException e1) {
					getLogger().warning("Unable to send datas to the SQL database.");
				}
		    }
	    }
	}

}
